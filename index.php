<?php

define('PATH_CONFIG', 'app/config/');
define('PATH_CONTROLLER', 'app/controller/');
define('PATH_MODEL', 'app/model/');
define('PATH_LIBRARY', 'app/libraries/');
define('PATH_VENDOR', 'vendor/');

define("BASE_PATH", "http://myapi.app");
require_once (PATH_CONFIG . 'definition.php');

// Make sure you have Composer's autoload file included
require (PATH_VENDOR . 'autoload.php');

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

/** Start Your Controller **/
require ('app/routes.php');

