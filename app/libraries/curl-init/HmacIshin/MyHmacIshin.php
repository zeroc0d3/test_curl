<?php

define("ORIGIN", "https://dbz.space");
define("REFERER", "https://dbz.space/tools/identifier-encryption");
define("USER_AGENT", "Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36"); // Samsung Galaxy S6

class MyHmacIshin
{
    private $url;
    private $key_auth;
    private $response;
    private $status;
    private $method_request;
    private $method_request_name;
    private $method_auth;
    private $method_auth_name;
    private $data_request;
    private $ch;
    private $message;
    private $result;
    
    /**
    * Initialization from array data request
    * @param    string      $url               
    * @param    string      $method_request    
    * @param    string      $method_auth       
    * @param    string      $key_auth          
    * @param    array/json  $data_request      
    * @return   json        Curl initialization for restapi
    */
    public static function curlInit($url, $method_request, $method_auth, $key_auth, $data_request) 
    {
        $result = null;
        $response = null;

        $ch = curl_init($url);

        if ($method_request == METHODE_REQUEST_POST) 
        {
            $method_request_name = "POST";
        } else if ($method_request == METHODE_REQUEST_GET) 
        {
            $method_request_name = "GET";
        } else if ($method_request == METHODE_REQUEST_PUT) 
        {
            $method_request_name = "PUT";
        } else 
        {
            $method_request_name = "DELETE";
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method_request_name);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($method_auth == METHODE_AUTH_BASIC) {
            $method_auth_name = "Basic ";
        } else if ($method_auth == METHODE_AUTH_BEARER) {
            $method_auth_name = "Bearer ";
        } else {
            $method_auth_name = "";
        }

        if (!empty($key_auth) or $key_auth != null) 
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                    'Accept-Encoding: gzip, deflate, br',
                    'Accept-Language: en-US,en;q=0.8',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Referer: '.REFERER,
                    'User-Agent: '.USER_AGENT,
                    'Origin: '.ORIGIN,
                    'Authorization: ' . $method_auth_name . $key_auth
                )
            );
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                    'Accept-Encoding: gzip, deflate, br',
                    'Accept-Language: en-US,en;q=0.8',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'Referer: '.REFERER,
                    'User-Agent: '.USER_AGENT,
                    'Origin: '.ORIGIN
                )
            );
        }    

        if (!empty($data_request) or $data_request != null) 
        {
            $data_request_json = json_encode($data_request);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_request_json);
        }

        //var_dump($url, $method_request, $method_auth, $key_auth, $data_request, $method_auth_name); die();
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($status != 200) {
            $result = "ERROR";
        } else {
            $result = $response;
        }
        return $result;
    }

    /**
    * Initialization from JSON datarequest
    * @param    string      $url               
    * @param    string      $method_request    
    * @param    string      $method_auth       
    * @param    string      $key_auth          
    * @param    array/json  $data_request      
    * @param    boolean     $is_json      
    * @return   json        Curl initialization for restapi
    */
    public static function curlInitJson($url, $method_request, $method_auth, $key_auth, $data_request, $is_json) 
    {
        $result = null;
        $response = null;

        $ch = curl_init($url);

        if ($method_request == METHODE_REQUEST_POST) 
        {
            $method_request_name = "POST";
        } else if ($method_request == METHODE_REQUEST_GET) 
        {
            $method_request_name = "GET";
        } else if ($method_request == METHODE_REQUEST_PUT) 
        {
            $method_request_name = "PUT";
        } else 
        {
            $method_request_name = "DELETE";
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method_request_name);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($method_auth == METHODE_AUTH_BASIC) {
            $method_auth_name = "Basic ";
        } else if ($method_auth == METHODE_AUTH_BEARER) {
            $method_auth_name = "Bearer ";
        } else {
            $method_auth_name = "";
        }

        if (!empty($key_auth) or $key_auth != null) 
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                    'Accept-Encoding: gzip, deflate, br',
                    'Accept-Language: en-US,en;q=0.8',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Referer: '.REFERER,
                    'User-Agent: '.USER_AGENT,
                    'Origin: '.ORIGIN,
                    'Authorization: ' . $method_auth_name . $key_auth
                )
            );
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                    'Accept-Encoding: gzip, deflate, br',
                    'Accept-Language: en-US,en;q=0.8',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Referer: '.REFERER,
                    'User-Agent: '.USER_AGENT,
                    'Origin: '.ORIGIN
                )
            );
        }    

        if (!empty($data_request) or $data_request != null) 
        {
            if (!$is_json == true) {
                $data_request_json = json_encode($data_request);
            }
            $data_request_json = $data_request;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_request_json);
        }

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($status != 200) {
            $result = "ERROR";
        } else {
            $result = $response;
        }
        return $result;
    }

    /**
    * Create post response
    * @param    string      $url               
    * @param    string      $key_auth (null)         
    * @param    array/json  $data_request (null)     
    * @param    boolean     $is_json (true/false)     
    * @return   json        Curl get restapi
    */
    public static function curlPostResponse($url, $key_auth, $data_request, $is_json) 
    {
        return self::curlInitJson($url, METHODE_REQUEST_POST, null, null, $data_request, $is_json);
    }

    /**
    * Create basic authorization
    * @param    string  $url               
    * @param    string  $key_auth          
    * @return   json    Curl basic authorization
    */
    public static function curlBasic($url, $key_auth) 
    {
        return self::curlInit($url, METHODE_REQUEST_POST, METHODE_AUTH_BASIC, $key_auth, $data_request = null);
    }

    /**
    * Create post from bearer authorization
    * @param    string      $url               
    * @param    string      $key_auth          
    * @param    array/json  $data_request      
    * @return   json        Curl post restapi
    */
    public static function curlBearerPost($url, $key_auth, $data_request) 
    {
        return self::curlInit($url, METHODE_REQUEST_POST, METHODE_AUTH_BEARER, $key_auth, $data_request);
    }

    /**
    * Create get from bearer authorization
    * @param    string      $url               
    * @param    string      $key_auth          
    * @param    array/json  $data_request      
    * @return   json        Curl get restapi
    */
    public static function curlBearerGet($url, $key_auth, $data_request) 
    {
        return self::curlInit($url, METHODE_REQUEST_GET, METHODE_AUTH_BEARER, $key_auth, $data_request = null);
    }

    /**
    * Create put from bearer authorization
    * @param    string      $url               
    * @param    string      $key_auth          
    * @param    array/json  $data_request      
    * @return   json Curl   put restapi
    */
    public static function curlBearerUpdate($url, $key_auth, $data_request) 
    {
        return self::curlInit($url, METHODE_REQUEST_PUT, METHODE_AUTH_BEARER, $key_auth, $data_request);
    }

    /**
    * Create delete from bearer authorization
    * @param    string      $url               
    * @param    string      $key_auth          
    * @param    array/json  $data_request      
    * @return   json        Curl delete restapi
    */
    public static function curlBearerDelete($url, $key_auth, $data_request) 
    {
        return self::curlInit($url, METHODE_REQUEST_DELETE, METHODE_AUTH_BEARER, $key_auth, $data_request);
    }

}
