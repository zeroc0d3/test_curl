<?php

class MyTransaction 
{
    private $url;
    private $url_by_id;
    private $key_auth;
    private $data_request;
    private $id;
    private $response;
    private $status;
    private $ch;
    private $data;
    private $message;
    private $result;

    /**
     * MyTransaction module return message & data
     * @param   string  $message
     * @return  json    Return data from curl GET, POST, PUT, DELETE
     */
    public static function returnMessage($message) 
    {
        $result  = null;
        $data    = array();
        if ($message != "ERROR") {
            $data     = $message;
            $result   = $data;
        } else {
            $result   = "Error in bearer token !";
        }
        //print_r($result);die();
        return $result;
    }
    
    /**
     * MyTransaction module Post
     * @param   string  $url
     * @param   string  $key_auth
     * @param   string  $data_request
     * @return  json    Registration user mytrans
     */
    public static function mytransPost($url, $key_auth, $data_request) 
    {
        $message = \MyCurl::curlBearerPost($url, $key_auth, $data_request);
        return self::returnMessage($message);
    }

    /**
     * MyTransaction module Get
     * @param   string  $url
     * @param   string  $key_auth     
     * @param   string  $data_request 
     * @return  json    Get list all products 
     */
    public static function mytransGet($url, $key_auth, $data_request) 
    {
        $message = \MyCurl::curlBearerGet($url, $key_auth, $data_request = null);
        return self::returnMessage($message);
    }

    /**
     * MyTransaction module Update
     * @param   string  $url
     * @param   string  $key_auth     
     * @param   string  $data_request 
     * @return  json    Update item from cart
     */
    public static function mytransUpdate($url, $key_auth, $data_request) 
    {
        $message = \MyCurl::curlBearerUpdate($url, $key_auth, $data_request);
        return self::returnMessage($message);
    }
    
    /**
     * MyTransaction module Delete
     * @param   string  $url
     * @param   string  $key_auth     
     * @param   string  $data_request 
     * @return  json    Delete item from cart
     */
    public static function mytransDelete($url, $key_auth, $data_request) 
    {
        $message = \MyCurl::curlBearerDelete($url, $key_auth, $data_request);
        return self::returnMessage($message);
    }

    /**
     * MyTransaction module Post Response
     * @param   string  $url
     * @param   string  $key_auth     
     * @param   string  $data_request 
     * @return  json    Respond payment
     */
    public static function postResponse($url, $key_auth, $data_request, $is_json) 
    {
        $message = \MyCurl::curlPostResponse($url, $key_auth, $data_request, $is_json);
        return self::returnMessage($message);
    }   

}
