<?php

class MyToken 
{
    private $client_id;
    private $client_secret;
    private $text_to_encode;
    private $url;
    private $str_base64_token;
    private $response;
    private $status;
    private $ch;
    private $data;
    private $message;
    private $result;

    /**
     * Create base64 encode client_id & client_secret
     * @param   string  $client_id
     * @param   string  $client_secret
     * @return  string  Create base64 encoded string
     */
    public static function base64BasicToken($client_id, $client_secret) 
    {
        $result         = null;
        $text_to_encode = $client_id . ":" . $client_secret;
        $result         = base64_encode($text_to_encode);
        return $result;
    }

    /**
     * Create basic token header
     * @param   string  $url               
     * @param   string  $str_base64_token
     * @return  array   Create basic token header authorization
     */
    public static function basicAccessToken($url, $str_base64_token) 
    {
        $result  = null;
        $data    = array();
        $message = \MyCurl::curlBasic($url, $str_base64_token);

        if ($message != "ERROR") {
            /*
             {
                "access_token": "6f14be4e3100d2a00d995d247cfa4c6c6e30b63c",
                "expires_in": 2628000,
                "token_type": "Bearer"
              }
             */
            $data     = json_decode($message, true);
         // $result   = $data["access_token"]; // Only "access_token"
            $result   = $data;
        } else {
            $result   = "Error in token basic auth !";
        }

        return $result;
    }
    
    /**************************** 
     *  Generate Oauth Token    *
     ****************************/
    /**
     * Create oauth token
     * @return  array   Create oauth token bearer authorization
     */
    public static function createOauthToken() {
        $str_base64_token = self::base64BasicToken(CLIENT_ID, CLIENT_SECRET);
        return self::basicAccessToken(URL_TOKEN, $str_base64_token);
    }

}
